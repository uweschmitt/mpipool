.. :changelog:

History
-------

0.2.1 (2019-05-21)
++++++++++++++++++

* Better output in traceback when client raises and exception.

0.2.0 (2019-05-16)
++++++++++++++++++

* Moved initialization of walkers to Pool.__init__

0.1.1 (2019-05-16)
++++++++++++++++++

* Fixed setup.py, previous version created empty packages.

0.1.0 (2019-05-10)
++++++++++++++++++

* First release on PyPI.
